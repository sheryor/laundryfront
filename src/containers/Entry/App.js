import React, { Component } from 'react';
import { MainContainer, Header, LogoSpan, Card } from './App.style';
import logo from '../../assets/logo.png';
import wash from '../../assets/wash.png';
import Services from '../../components/Services/Services';
import Dashboard from '../../components/Dashboard/Dashboard';
import Bucket from '../../components/Bucket/Bucket';

class App extends Component {
  state = {
    showServices: false,
    tableShow: false,
    priceList: null,
    currentPriceList: null,
    buyCard: null,
    isChildClothes: false,
    isExpress: false,
    isSlow: false,
    totalSum: 0
  }

  componentDidMount() {
    fetch(`//localhost:8080/v1/allprices`)
      .then( res => res.json())
      .then( res => this.setState({ priceList: res}))
  }


  buy = () => {
    const respBody = {
      order: this.state.buyCard,
      optional: {
        isExpress: this.state.isExpress,
        isSlow: this.state.isSlow,
        isChildClothes: this.state.isChildClothes,
      }
    }
    
    fetch(`//localhost:8080/v1/checkout`, {
      method: "POST",
      body: JSON.stringify(respBody)
    })
    .then( res => res.json())
    .then( res => this.setState({ totalSum: res.totalSum}));
  }

  expressServe = evt => {
    let isExpress = true;
    this.setState({  isExpress, isSlow: false });
  }

  slowServe = evt => {
    let isSlow = true;
    this.setState({ isSlow, isExpress: false });
  }
  

  showServices = () => this.setState({ showServices: true })

  childClothes = evt => {
    let isChildClothes = true;
    this.setState({  isChildClothes });
  } 

  addToCart = ( item ) => {
    item.count = item.count || 0;
    item.sum = (item.count * item.price);
    let buyCard = this.state.buyCard && this.state.buyCard ? this.state.buyCard : [];
    
    const idx = buyCard.findIndex( itm => item.desc === itm.desc);

    if (idx >= 0) {
      if (item.sum === 0 ) {
       buyCard.splice(idx,1)
      } else {
        buyCard[idx].sum = item.sum;
        buyCard[idx].count = item.count;
      }
    } else if (item.sum > 0 ) {
      buyCard.push(item);
    }

    this.setState({ buyCard });
  }

  changeCount = ( item , val ) => {
    item.count = isNaN(item.count) ? 0: item.count;
    item.count = val === -1 && item.count > 0 ? --item.count: val === 1 ? ++item.count: item.count;
    this.setState(this.state)
  }

  showService = name => {
    const data = {...this.state.priceList};
    this.setState({ currentPriceList: data[name], tableShow: true})
  }

  render() {
    return (
      <React.Fragment>
        <Header>
          <img 
            src={logo} 
            alt="Либосшуй" 
            height="80px"
          />
          <LogoSpan>Либосшуй</LogoSpan>
        </Header>
        <MainContainer>
            <Card onClick={this.showServices}>
              <img width="80px" src={wash} alt="Наши услуги" />
              Наши Услуги
            </Card>
        </MainContainer>
        <Services 
          isVisible={this.state.showServices}
          showService={this.showService}
        />
        <Bucket 
          buy={this.buy}
          buyList={this.state.buyCard}
          express={this.expressServe}
          slow={this.slowServe}
          childClothes={this.childClothes}
          totalSum={this.state.totalSum}
        />
        <Dashboard 
          isVisible={this.state.tableShow} 
          data={this.state.currentPriceList}
          addToCart={this.addToCart}
          changeCount={this.changeCount}
        />
      </React.Fragment>
    );
  }
}

export default App;
