import styled from 'styled-components';

const Header = styled.header`
    width: 600px;
    margin: 10px auto;
`;
const LogoSpan = styled.span`
    color: #5ebed6;
    font-size: 36px;
    vertical-align: top;
    margin-left:50px;
`;

const MainContainer = styled.div`
    width: 1000px;
    display: flex;
`;

const Card = styled.div`
    margin-left: 20px;
    width: 250px;
    height: 150px;
    border: 1px solid #eee;
    box-shadow: 1px 1px 3px #ccc;
    font-size: 20px;
    padding: 10px;
    &:hover {
        cursor: pointer;
        box-shadow:4px 4px 4px #ccc;
    }
`;



const Button = styled.div`
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor:pointer;
    
    &:hover {
        background: #008080;
    }
    
    &:active {
        background:#4CAF50;
    }
`;
export { MainContainer, Header, LogoSpan, Card, Button }