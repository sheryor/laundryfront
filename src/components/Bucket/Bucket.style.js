import styled from 'styled-components';

const Bucket = styled.div`
    border:2px solid black;
    width: 300px;
    position: absolute;
    top:  10px;
    right: 10px;

    ul{
        list-style: none;
    }

    ul li {
        border-bottom: 1px dashed black;
    }
`;
const Button = styled.div`
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    cursor:pointer;
    
    &:hover {
        background: #008080;
    }
    
    &:active {
        background:#4CAF50;
    }
`;

export { Bucket, Button };