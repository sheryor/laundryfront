import React from 'react';
import { Bucket, Button } from './Bucket.style';
import { guid } from '../../utils/guid';
import bucketPay from '../../assets/buy.png';

const bucket = props => (
    <Bucket>
        <h4 align="center">ООО Либосшуй</h4>
        <h5 align="center">Квитанция к оплате</h5>
        <ul>
            {props.buyList && props.buyList.length > 0 ?
                props.buyList.map( item => (
                    <li key={guid()}>{item.desc}   {item.price} * {item.count} = {item.sum}</li> ))
                    :null}
        </ul>
        <p><input type="checkbox" onChange={props.childClothes} />Детские вещи</p>
        <p><input type="radio" name="r1" onChange={props.slow} />     Я не спешу и подожду 5 и более дней</p>
        <p><input type="radio" name="r1" onChange={props.express} />     Экспресс</p>
        
        <h5 align="right">Итого: {props.totalSum.toFixed(2)} сом</h5>
        <Button onClick={props.buy}>
            <img src={bucketPay} width="20px" alt="Оплатить" />
            <span style={{marginLeft: "10px"}}>Просчитать</span>
        </Button>
    </Bucket> 
);

export default bucket;