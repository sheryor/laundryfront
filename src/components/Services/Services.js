import React from 'react';
import { Card, MainContainer } from '../../containers/Entry/App.style';
import chemistClean from '../../assets/khimchistka.png';
import glazhka from '../../assets/glazhka.png';
import dirty from '../../assets/dirty.png';
import general from '../../assets/strika.jpg';

const services = props => (
    props.isVisible ?
        <MainContainer>
            <Card onClick={ () => props.showService('laundryPriceList')}>
                <img  width="120px" src={chemistClean} alt="Химчистка" />
                </Card>
            <Card onClick={ () => props.showService('generalPriceList')}>
                <img width="80px" src={general} alt="Общие услуги" />
                Общие услуги
            </Card>
            <Card onClick={ () => props.showService('ironingPriceList')}>
                <img  width="80px" src={glazhka} alt="  Гладильные услуги" />
                Гладильные услуги
            </Card>
            <Card onClick={ () => props.showService('stainRemovalPriceList')}>
                <img width="80px" src={dirty} alt="Удаление пятен" />
                Удаление пятен
            </Card>
        </MainContainer>
        : null  
);

export default services;