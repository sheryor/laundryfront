import React from 'react';
import { guid } from '../../utils/guid';
import { Button } from '../../containers/Entry/App.style';

const dashboard = props => (
    props.isVisible ? 
        <table style={{margin: "0 auto"}}>
            <thead>
                 <tr>
                    <th>Услуга</th>
                    <th>Количество вещей(шт/кг.)</th>
                    <th>Цена</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                {props.data ? 
                    props.data.map( item => <tr key={guid()}>
                        <td>{item.desc}</td>
                        <td> 
                            <Button 
                                style={{padding: "5px"}}
                                onClick={() => props.changeCount(item, -1)}
                            >-</Button>  {item.count || 0} <Button style={{padding: "5px"}}  onClick={() => props.changeCount(item, 1)}>+</Button></td>
                        <td>{item.price}</td>
                        <td><Button onClick={ () => props.addToCart(item)}>Добавить</Button></td>
                    </tr> )    
                :null}
            </tbody>
        </table>
    : null
);

export default dashboard;